
#include "RocketProjectile.h"

Texture* RocketProjectile::r_pTexture = nullptr; //Rocket Texture

RocketProjectile::RocketProjectile()
{

	SetSpeedRocket(150);
	SetDamageRocket(50);
	//SetDirectionRocket(-Vector2::ONE);
	SetCollisionRadius(75);

	m_drawnByLevel = true;
}

void RocketProjectile::Update(const GameTime* pGameTime)
{
	if (IsActive())
	{
		//Changing the x axis
		float x = sin(pGameTime->GetTotalTime() * Math::PI + GetIndex());
		x *= m_speed * pGameTime->GetTimeElapsed() * 2.5f;
		TranslatePosition(x, m_speed * -pGameTime->GetTimeElapsed());
		
		Vector2 position = GetPosition();
		Vector2 size = r_pTexture->GetSize();

		// Is the projectile off the screen?
		if (position.Y < -size.Y) Deactivate();
		else if (position.X < -size.X) Deactivate();
		else if (position.Y > Game::GetScreenHeight() + size.Y) Deactivate();
		else if (position.X > Game::GetScreenWidth() + size.X) Deactivate();
	}

	GameObject::Update(pGameTime);
}

void RocketProjectile::Draw(SpriteBatch* pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(r_pTexture, GetPosition(), Color::White, r_pTexture->GetCenter());
	}
}

void RocketProjectile::ActivateRocket(const Vector2& position, bool wasShotByPlayer)
{
	m_wasShotByPlayer = wasShotByPlayer;
	SetPosition(position);

	GameObject::Activate();
}

std::string RocketProjectile::ToString() const
{
	return ((WasShotByPlayerRocket()) ? "Player " : "Enemy ") + GetProjectileTypeStringRocket();
}

CollisionType RocketProjectile::GetCollisionType() const
{
	CollisionType shipType = WasShotByPlayerRocket() ? CollisionType::PLAYER : CollisionType::ENEMY;
	return (shipType | GetProjectileTypeRocket());
}