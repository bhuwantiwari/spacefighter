
#pragma once

#include "KatanaEngine.h"
#include "Projectile.h"
#include "TriggerType.h"
//Including the rocket projectile
#include "RocketProjectile.h"

using namespace KatanaEngine;

class Weapon
{

public:
	
	Weapon(bool isActive = true)
	{
			SetTriggerType(TriggerType::PRIMARY);
			SetTriggerTypeRocket(TriggerType::SECONDARY);
			//SetTriggerType(TriggerType::SECONDARY);
		
		
		m_isActive = isActive;
		
	}
	
	virtual ~Weapon() { }

	virtual void Update(const GameTime *pGameTime) { };

	virtual void Draw(SpriteBatch *pSpriteBatch) { };

	virtual void Fire(TriggerType triggerType) = 0; //Pure Virtual Function

	virtual void SetGameObject(GameObject *pGameObject) { m_pGameObject = pGameObject; }

	virtual void SetOffset(Vector2 offset) { m_offset = offset; }

	virtual void SetTriggerType(TriggerType triggerType) { m_triggerType = triggerType; }
	//Making a rocket SetTriggerType
	virtual void SetTriggerTypeRocket(TriggerType triggerType) { m_rtriggerType = triggerType; }
	//Allowing the SetProjecttile to have 2 parameters 

	virtual void SetProjectilePool(std::vector<Projectile*>* pProjectiles, std::vector<RocketProjectile*>* pRocketProjectiles) { m_pProjectiles = pProjectiles; m_pRocketProjectile = pRocketProjectiles; }

	virtual void Activate() { m_isActive = true; }

	virtual void Dectivate() { m_isActive = false; }

	virtual bool IsActive() const { return m_isActive && m_pGameObject->IsActive(); }


protected:

	virtual TriggerType GetTriggerType() const { return m_triggerType; }
	
	//Getting a rocket trigger type
	virtual TriggerType GetRocketTriggerType() const { return m_rtriggerType; }

	virtual Vector2 GetPosition() const { return m_pGameObject->GetPosition() + m_offset; }

	virtual Projectile *GetProjectile()
	{
		m_projectileIt = m_pProjectiles->begin();

		for (; m_projectileIt != m_pProjectiles->end(); m_projectileIt++)
		{
			Projectile *pProjectile = *m_projectileIt;
			if (!pProjectile->IsActive()) return pProjectile;
		}

		return nullptr;
	}
	//Creating Rocket Projectile and returning it 
	virtual RocketProjectile* GetRocketProjectile()
	{
		m_RocketprojectileIt = m_pRocketProjectile->begin();

		for (; m_RocketprojectileIt != m_pRocketProjectile->end(); m_RocketprojectileIt++)
		{
			RocketProjectile* pRocketProjectile = *m_RocketprojectileIt;
			if (!pRocketProjectile->IsActive()) return pRocketProjectile;
		}

		return nullptr;
	}




private:

	bool m_isActive;

	GameObject *m_pGameObject;

	Vector2 m_offset;

	TriggerType m_triggerType;

	//Set Rocket Trigger Member var
	TriggerType m_rtriggerType;

	std::vector<Projectile *>::iterator m_projectileIt;
	std::vector<Projectile *> *m_pProjectiles;	
	//Adding a vector iterator for rocket
	std::vector<RocketProjectile *>::iterator m_RocketprojectileIt;
	//A list of Rocket Projectile
	std::vector<RocketProjectile *> *m_pRocketProjectile;
	

};

