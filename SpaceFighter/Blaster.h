
#pragma once

#include "Weapon.h"

class Blaster : public Weapon
{

public:

	Blaster(const bool isActive) : Weapon(isActive)
	{
		m_cooldown = 0;
		m_cooldownSeconds = 0.20; //Changed the player cool down from 0.35 to 0.20
		//SetTriggerType(TriggerType::PRIMARY);
		//Set the rocket cooldown
		m_cooldownRocket = 2;

	}

	virtual ~Blaster() { }

	virtual void Update(const GameTime *pGameTime)
	{
		if (m_cooldown > 0) m_cooldown -= pGameTime->GetTimeElapsed();
	}

	virtual bool CanFire() const { return m_cooldown <= 0; } 

	virtual void ResetCooldown() { m_cooldown = 0; }

	virtual float GetCooldownSeconds() { return m_cooldownSeconds; }

	virtual void SetCooldownSeconds(const float seconds) { m_cooldownSeconds = seconds; }

	virtual void Fire(TriggerType triggerType)
	{
		//This must be overriden
		if (IsActive() && CanFire()) //If IsActive and CanFire = true
		{
			//Bunch of checks below
			if (triggerType.Contains(GetTriggerType()))
			{
				Projectile *pProjectile = GetProjectile();
				//RocketProjectile *pRocketProjectile = GetRocketProjectile();
				if (pProjectile)
				{
					
					//This is sending the fire to the enemey. The code above checks if it the user CAN fire
					pProjectile->Activate(GetPosition(), true);
					m_cooldown = m_cooldownSeconds;
				}
				/*
				//Creating a rocket and making sure the sure can fire it or not
				if (pRocketProjectile)
				{
				
					//SetTriggerType(TriggerType::SECONDARY);
					//pRocketProjectile->Activate(GetPosition(), true);
					pRocketProjectile->ActivateRocket(GetPosition(), true);
					//pProjectile->Activate(GetPosition(), true);
					m_cooldown = m_cooldownSeconds;
				}
				*/

			}
			if (triggerType.Contains(GetRocketTriggerType()))
			{
				RocketProjectile* pRocketProjectile = GetRocketProjectile();
				if (pRocketProjectile)
				{
					//SetTriggerType(TriggerType::SECONDARY);
					//pRocketProjectile->Activate(GetPosition(), true);
					pRocketProjectile->ActivateRocket(GetPosition(), true);
					//pProjectile->Activate(GetPosition(), true);
					//m_cooldown = m_cooldownSeconds;
					m_cooldown = m_cooldownRocket;
				}
			
			}

		}
	}


private:

	float m_cooldown;
	float m_cooldownSeconds;
	float m_cooldownRocket;

};