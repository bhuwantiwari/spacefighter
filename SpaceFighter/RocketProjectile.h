
#pragma once

#include "KatanaEngine.h"
#include "GameObject.h"

class RocketProjectile : public GameObject
{

public:

	RocketProjectile();
	virtual ~RocketProjectile() { }


	static void SetTextureRocket(Texture* pTexture) { r_pTexture = pTexture; }

	virtual void Update(const GameTime* pGameTime);

	virtual void Draw(SpriteBatch* pSpriteBatch);

	virtual void ActivateRocket(const Vector2& position, bool wasShotByPlayer = true);

	virtual float GetDamage() const { return m_damage; }

	virtual std::string ToString() const;

	virtual CollisionType GetCollisionType() const;

	virtual bool IsDrawnByLevel() const { return m_drawnByLevel; }

	virtual void SetManualDraw(const bool drawManually = true) { m_drawnByLevel = !drawManually; }


protected:

	virtual void SetSpeedRocket(const float speed) { m_speed = speed; }

	virtual void SetDamageRocket(const float damage) { m_damage = damage; }

	virtual void SetDirectionRocket(const Vector2 direction) { m_direction = direction;  }

	virtual float GetSpeedRocket() const { return m_speed; }

	virtual Vector2& GetDirectionRocket() { return m_direction; }

	virtual bool WasShotByPlayerRocket() const { return m_wasShotByPlayer; }

	virtual CollisionType GetProjectileTypeRocket() const { return CollisionType::PROJECTILE; }

	virtual std::string GetProjectileTypeStringRocket() const { return "Projectile"; }


private:


	//This is rocket texture
	static Texture* r_pTexture;

	float m_speed;
	float m_damage;

	Vector2 m_direction;

	bool m_wasShotByPlayer;

	bool m_drawnByLevel;
};

